//===-- test_dual.cpp - test duals/dual -------------------------*- C++ -*-===//
//
// Part of the cppduals Project
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
// (c)2019 Michael Tesch. tesch1@gmail.com
//
/**
 * just a place to play around and get dirty.
 *
 * (c)2019 Michael Tesch. tesch1@gmail.com
 */

#include <math.h>
#include <iostream>
#include <iomanip>

#include "type_name.hpp"
#include <duals/dual_eigen>
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <unsupported/Eigen/MatrixFunctions>

using std::cout;
using std::cerr;
using duals::dualf;
using duals::duald;
using duals::dualf;
using duals::duald;
using duals::dualld;
using duals::hyperdualf;
using duals::hyperduald;
using duals::hyperdualld;
typedef std::complex<double> complexd;
typedef std::complex<float> complexf;
typedef std::complex<duald> cduald;
typedef std::complex<dualf> cdualf;
typedef std::complex<hyperdualf> chyperdualf;

using duals::dual_traits;
using namespace duals::literals;

template <class eT, int N=Eigen::Dynamic, int P=N> using emtx = Eigen::Matrix<eT, N, P>;
template <class eT> using smtx = Eigen::SparseMatrix<eT>;

template <int N=2, int P=N> using ecf = Eigen::Matrix<complexf, N, P> ;
template <int N=2, int P=N> using edf = Eigen::Matrix<dualf, N, P> ;
template <int N=2, int P=N> using ecdf = Eigen::Matrix<cdualf, N, P> ;
#define PO_EXPR_TYPE(...) typename std::decay<decltype( __VA_ARGS__ )>::type::PlainObject


template <class T = int>
class Rando { T x; };

struct Randa { int x; };

namespace std {
template <class T>
struct common_type<Rando<T>,T> { typedef Rando<T> type; };
}

#if 0

int main(int argc, char * argv[])
{
  emtx<cduald,50> A,B,C;
  //emtx<complexd,50> A,B,C;
  C = A * B;
}

#elif 0
int main(int argc, char * argv[])
{
  typedef double T;
  T h(T(1) / (1ull << (std::numeric_limits<T>::digits / 3)));
#define func erfc
  for (double x = 0; x < 4; x += .21) {
    std::cout << " erf(" << x << ") = " << func (x) << " , =" << (func (x+h)- func (x))/h << "\n";
    std::cout << "d(erf" << x << ") = " << func (x + 1_e) << "\n";
  }
}

#else

template <class T> T f(T x) { return pow(x,pow(x,x)); }
template <class T> T df(T x) { return pow(x,-1. + x + pow(x,x)) * (1. + x*log(x) + x*pow(log(x),2.)); }

int main(int argc, char * argv[])
{
  dualf h;
  dualf x(1);
  hyperdualf y;
  hyperdualf w(1);
  hyperdualf z(x,h);
  hyperdualf a(x);
  emtx<double> ed;
  emtx<float> ef;
  emtx<complexd> ecd;
  emtx<complexf> ecf_;
  emtx<duald> edd;
  emtx<dualf> edf_;
  emtx<cduald> ecdd;
  emtx<cdualf> ecdf_;

  std::cout << " f(2.)            = " << f(2.)    << "\n";
  std::cout << "df(2.)            = " << df(2.)   << "\n";
  std::cout << " f(2+1_e)         = " << f(2+1_e) << "\n";
  std::cout << " f(2+1_e).dpart() = " << f(2+1_e).dpart() << "\n";

  x = h;
  a = y;
  std::cout << x << "\n" << h << "\n";
  std::cout << w << "\n" << "unitx:" << Eigen::Vector2f::UnitX() << "\n";
  std::cout << z << "\n" << "unity:" << Eigen::Vector2f::UnitY() << "\n";;

  std::cout << type_name<duals::promote<double,complexf>::type>() << "\n";
  std::cout << type_name<duals::promote<dualf,complexf>::type>() << "\n";
  std::cout << type_name<duals::promote<dualf,cdualf>::type>() << "\n";
  std::cout << type_name<duals::promote<dualf,chyperdualf>::type>() << "\n";
  std::cout << type_name<duals::promote<complexf,dualf>::type>() << "\n";
  std::cout << "f-bool:" << type_name<duals::promote<float,std::true_type>::type>() << "\n";
  complexf cf;
  std::cout << type_name<decltype(cf)>() << "\n";
  std::cout << "ecf:" << type_name<decltype(ecf_)>() << "\n";
  std::cout << "ecf:" << type_name<
    typename Eigen::internal::scalar_product_op<complexf,int>>() << "\n";
  std::cout << "dpart(ecdf_)" << type_name<decltype((dpart(ecdf_)).derived())>() << "\n";
  std::cout << "ecf*1:" << type_name<decltype((ecf_ * 1).derived())>() << "\n";
  std::cout << "dpart(ecdf_) po:"
            << type_name<typename std::decay<decltype((rpart(ecdf_)).matrix())>::type::PlainObject>() << "\n";
  std::cout << "ecf*1 po:"
            << type_name<typename std::decay<decltype((ecf_ * 1).matrix())>::type::PlainObject>() << "\n";
  std::cout << "edf:" << type_name<decltype(edf_)>() << "\n";
  std::cout << "edf*1:" << type_name<decltype((edf_ * 1).derived())>() << "\n";
  std::cout << "edf*1 po:"
            << type_name<typename std::decay<decltype((edf_ * 1).matrix())>::type::PlainObject>() << "\n";

  std::cout << type_name<decltype(1_ef)>() << "\n";
  std::cout << type_name<decltype(cf + 1_ef)>() << "\n";
  std::cout << "a+h2 " << type_name<decltype(cf + hyperduald(2))>() << "\n";
  std::cout << "d:"
            << dual_traits<decltype(cf)>::depth << ", "
            << dual_traits<decltype(1_ef)>::depth << "\n";
  std::cout << "---\n";
  auto xx = cf + 1_ef;
  std::cout << "---\n";
  std::cout << xx << "---\n";
  //std::cout << "ecf*edf_:" << type_name<PO_EXPR_TYPE(ecf_ * edf_)>() << "\n";

  std::cout << edf<3>::Constant(3,3, 5 - 8_ef) << "\n";

  std::cout << ecf<3>::Random() + 1 * ecf<3>::Random() << "\n";
  std::cout << ecf<3>::Random() + complexf(1,2) * ecf<3>::Random() << "\n";
  std::cout << edf<3>::Random() + 1_ef * edf<3>::Random() << "\n";
  std::cout << "has ct:" << duals::detail::has_member_type<Randa, int>::value << "\n";
  //std::cout << "has ct:" << duals::detail::has_common_type<decltype(1_ef),
  //                                                         decltype(edf<3>::Random())>::value << "\n";
  std::cout << ecdf<3>::Random(3,3) << "\n";
  auto xyz = ecdf<6,1>::LinSpaced(6,cdualf(3+4_ef,3+4_ef), cdualf(1-5_ef,1-5_ef) );
  std::cout << xyz << "\n-\n";
  std::cout << conj(xyz.array()) << "\n";
  std::cout << "iscomplex? " << Eigen::NumTraits<cdualf>::IsComplex << "\n";
#if 0
  using Eigen::internal::Packet2cf;
  using Eigen::internal::pload;

  std::complex<duals::dual<float>> cd1 = cdualf(1+2_ef,3+4_ef);
  std::complex<duals::dual<float>> cd2 = cdualf(5+6_ef,7+8_ef);
  //std::complex<duals::dual<float>> cd1 = ecdf<1>::Random(1,1)(0) + 1_ef * ecdf<1>::Random(1,1)(0);
  //std::complex<duals::dual<float>> cd2 = ecdf<1>::Random(1,1)(0) + 1_ef * ecdf<1>::Random(1,1)(0);
  std::complex<duals::dual<float>> cd3;
  Packet1cdf p1 = pload<Packet1cdf>(&cd1);
  Packet1cdf p2 = pload<Packet1cdf>(&cd2);
  Packet1cdf p3;
  p3 = pconj(p1);
  pstore(&cd3, p3);
  std::cout << "conj=" << cd3 << "\n";
  p3 = pnegate(p1);
  pstore(&cd3, p3);
  std::cout << "nega=" << cd3 << "\n";
  p3 = pdiv(p1,p2);
  pstore(&cd3, p3);
  std::cout << "div =" << cd3 << "\n";
  std::cout << cd1 << "*\n" << cd2 << " = \n" << cd1 / cd2 << "\n";
  //std::cout << cd1 << "*" << cd2 << " = " << cd3 << "\n";
#endif
#if 1
  using Eigen::internal::Packet4df;
  using namespace Eigen::internal;
  std::vector<cdualf,Eigen::aligned_allocator<cdualf>> da(4), db(4), dc(4);
  da[0] = 1 + 2_ef;
  da[0] = duals::randos::random2<cdualf>();
  da[1] = 3 + 4_ef;
  da[2] = 5 + 6_ef;
  da[3] = 7 + 8_ef;
  db[0] = 1.1 + 2.2_ef;
  db[0] = duals::randos::random2<cdualf>();
  db[1] = 3.3 + 4.4_ef;
  db[2] = 5.5 + 6.6_ef;
  db[3] = 7.7 + 8.8_ef;

  //std::complex<duals::dual<float>> cd1 = ecdf<1>::Random(1,1)(0) + 1_ef * ecdf<1>::Random(1,1)(0);
  //std::complex<duals::dual<float>> cd2 = ecdf<1>::Random(1,1)(0) + 1_ef * ecdf<1>::Random(1,1)(0);

  Packet2cdf p1 = pload<Packet2cdf>(&da[0]);
  Packet2cdf p2 = pload<Packet2cdf>(&db[0]);
  Packet2cdf p3;
  p3 = pmul(p1,p2);
  pstore(&dc[0], p3);
  for (int i = 0; i < 4; i++) {
    std::cout << "conj =" << da[i] << "*" << db[i] << " = " << dc[i] << "\n";
    //std::cout << da[i] << ". " << " = " << conj(da[i]) << " err=" << 0 << "\n";
    //std::cout << da[i] << "." << db[i] << " = " << da[i]/db[i] << " err=" << (dc[i] - da[i]/db[i]) << "\n";
  }
#endif
#if 1
  std::stringstream Xx("(0.24151021242141724+0.31506165862083435_ef)");
  std::stringstream Yy("(3.9031729102134705e-06+0.06223597377538681_ef)");
  dualf xxxx, yyyy;
  Xx >> xxxx;
  Yy >> yyyy;
  std::cout << std::setprecision(10) << "x=" << xxxx << " y=" << yyyy << "\nx*y=" << xxxx / yyyy << "\n";
#endif
}

#endif
